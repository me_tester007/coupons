package com.project.constants;

/**
 * XML related INFO / DETAILS
 * */
public class XmlDB {
	//---XML RELATED DATA
	public static final String XMLDB_DIR = "xmldb";
	public static final String COMPANY_XMLFILE = XMLDB_DIR + "/companiesDB.xml";;
	public static final String CUSTOMER_XMLFILE = XMLDB_DIR + "/customersDB.xml";;
	public static final String COUPON_XMLFILE = XMLDB_DIR + "/couponsDB.xml";
	public static final String INBOX_XMLFILE = XMLDB_DIR + "/inboxesDB.xml";


	public static final String COMPANIES_ELEMENT = "companies";
	public static final String CUSTOMERS_ELEMENT = "customers";	
	public static final String COUPONS_ELEMENT = "coupons";
	public static final String INBOXES_ELEMENT = "inboxes";
	public static final String LOGS_ELEMENT = "logs";
	
	public static final String COMPANY_ELEMENT = "company";
	public static final String CUSTOMER_ELEMENT = "customer";	
	public static final String COUPON_ELEMENT = "coupon";
	public static final String INBOX_ELEMENT = "inbox";
	
	public static  String[] XML_FILES = {
			COMPANY_XMLFILE, CUSTOMER_XMLFILE,
			COUPON_XMLFILE, INBOX_XMLFILE
	};
	
}
