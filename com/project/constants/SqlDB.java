package com.project.constants;

/**
 * SQL related INFO / DETAILS
 * */
public class SqlDB {

	private SqlDB() {}
	
	//---SQL RELATED DATA
	public static String DBVENDOR;
	public static String HOST;
	public static int PORT;
	public static String USERNAME;
	public static String PASSWORD;
	public static String URL;
	public static String NAME;
	public static String DRV_NAME;
	public static String DB_INSTALL_TYPE;
	
	public static final String COMPANY_TABLE = "company";
	public static final String CUSTOMER_TABLE = "customer";	
	public static final String COUPON_TABLE = "coupon";
	public static final String INBOX_TABLE = "inbox";
	public static final String LOGS_TABLE = "logs";
	
	public static final String COMPANY_COUPON_TABLE = "company_coupon";
	public static final String CUSTOMER_COUPON_TABLE = "customer_coupon";
		
	public static final String COMPANY_INBOX_TABLE = "company_inbox";	
	public static final String CUSTOMER_INBOX_TABLE = "customer_inbox";

	public static final String COMPID_COLUMN = "comp_id";
	public static final String CUSTID_COLUMN = "cust_id";
	public static final String COUPONID_COLUMN = "coupon_id";
	public static final String INBOXID_COLUMN = "inbox_id";
	
	public static final String[] TABLE_NAMES = {
			COMPANY_COUPON_TABLE, CUSTOMER_COUPON_TABLE, COMPANY_INBOX_TABLE, CUSTOMER_INBOX_TABLE,
			COMPANY_TABLE, CUSTOMER_TABLE, COUPON_TABLE, INBOX_TABLE
	};
	
	public static final String SQLSCRIPT_DIR = "sqldb_scripts";
	public static final String[] TABLE_SQL_FILES = {
			SQLSCRIPT_DIR + "/company_coupon.sql",
			SQLSCRIPT_DIR + "/customer_coupon.sql",
			SQLSCRIPT_DIR + "/company_inbox.sql",
			SQLSCRIPT_DIR + "/customer_inbox.sql",
			SQLSCRIPT_DIR + "/company.sql",
			SQLSCRIPT_DIR + "/customer.sql",
			SQLSCRIPT_DIR + "/coupon.sql",
			SQLSCRIPT_DIR + "/inbox.sql",
	};
	
	public static void buildFullUrl() {
		URL = "jdbc:" + DBVENDOR + "://" + HOST + ":" + PORT + "/" + NAME;
	}
	
	public static String getGeneralUrl(){
		return "jdbc:" + DBVENDOR + "://" + HOST + ":" + PORT + "/";
	}
}
