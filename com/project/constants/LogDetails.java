package com.project.constants;

import com.project.daofile.LogLevel;

/**
 * Represents the Log Level for Logger
 * initialized at system startup read from properties files
 * */
public class LogDetails {

	private LogDetails(){}
	
	public static final LogLevel INFO 		= LogLevel.INFO;
	public static final LogLevel WARNING 	= LogLevel.WARNING;
	public static final LogLevel DEBUG 		= LogLevel.DEBUG;
	public static final LogLevel ERROR 		= LogLevel.ERROR;
	public static final LogLevel FATAL 		= LogLevel.FATAL;
	
	public static LogLevel currentLogLevel;
	
}
