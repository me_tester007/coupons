package com.project.constants;

/**
 * Constants for initial IDs in case objects do not exist in DB
 * */
public class Counters {

	private Counters() {}

	//---Static ID Counters
	public static final int companyIDCount = 1000;
	public static final int customerIDCount = 100;
	public static final int couponIDCount = 10;
	public static final int inboxIDCount = 10;

}
