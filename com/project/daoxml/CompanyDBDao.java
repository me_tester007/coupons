package com.project.daoxml;

import java.util.Collection;
import java.util.HashSet;

import com.project.beans.Accessory;
import com.project.beans.BeanClassType;
import com.project.beans.Company;
import com.project.beans.Coupon;
import com.project.beans.Inbox;
import com.project.beans.Owner;
import com.project.dao.CompanyDao;
import com.project.constants.XmlDB;

/**
* DAO Layer Class working with XML DB server
* */
public class CompanyDBDao implements CompanyDao {
	
	// Constructor
	public CompanyDBDao() {}

	// Creates a new company in the XMl file & checks if theres 
	// a company with the same id to avoid duplications
	@Override
	public void createCompany( Company company ) {
		
		DaoXmlHandler.createOwner(company, XmlDB.COMPANIES_ELEMENT, XmlDB.COMPANY_XMLFILE, BeanClassType.COMPANY);
	}

	// removes the given company and update the XMl file.
	@Override
	public void removeCompany( Company company ) {
		
		DaoXmlHandler.removeOwner(company, XmlDB.COMPANY_XMLFILE, BeanClassType.COMPANY);
	}

	// updates the given company by removing it and create it again in the file
	@Override
	public void updateCompany( Company c ) {
		removeCompany(c);
		createCompany(c);
	}
	
	// returns a company object from the xml file by name
	@Override
	public Company getCompany( String name ) {
		
		return (Company)DaoXmlHandler.getOwner(name, XmlDB.COMPANY_XMLFILE, BeanClassType.COMPANY);	
	}
	
	// returns a company object from the xml file by id
	@Override
	public Company getCompany( int id ) {
		
		return (Company)DaoXmlHandler.getOwner(id, XmlDB.COMPANY_XMLFILE, BeanClassType.COMPANY);
	}

	// returns all of the companies in a company collection from the xml file 
	@Override
	public Collection<Company> getAllCompanies() {
		
		Collection<Owner> owners = DaoXmlHandler.getAllOwners(XmlDB.COMPANY_XMLFILE, BeanClassType.COMPANY);
		Collection<Company> companies = new HashSet<>();
		
		for (Owner owner : owners)
			companies.add((Company)owner);

		return companies;
	}

	// returns all of the companies coupons by id in a coupon collection from the xml file	
	@Override
	public Collection<Coupon> getCoupons( int companyID ) {	
		
		Collection<Accessory> accessories = DaoXmlHandler.getCoupons(companyID, XmlDB.COMPANY_XMLFILE, XmlDB.COUPONS_ELEMENT, BeanClassType.COUPON);
		Collection<Coupon> coupons = new HashSet<>();
		
		for (Object object : accessories)
			coupons.add((Coupon)object);

		return coupons;
	}

	// returns all of the companies messages by id in a inbox collection from the xml file
	@Override
	public Collection<Inbox> getInboxMessages( int companyID ) {
			
		Collection<Accessory> accessories = DaoXmlHandler.getInboxMessages(companyID, XmlDB.COMPANY_XMLFILE, XmlDB.INBOX_ELEMENT);
		Collection<Inbox> inboxes = new HashSet<>();
		
		for (Accessory accessory : accessories)
			inboxes.add((Inbox)accessory);
		
		return inboxes;
	}

	
	@Override
	public boolean login( String companyName, String password ) {
		return DaoXmlHandler.login(XmlDB.COMPANY_XMLFILE, companyName, password);
	}

	@Override
	public int getCompanyLatestID() {
		
		return DaoXmlHandler.getLatestID(XmlDB.COMPANY_XMLFILE);
	}
	

}