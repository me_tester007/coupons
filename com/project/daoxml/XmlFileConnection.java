package com.project.daoxml;

import java.io.File;

import com.project.constants.XmlDB;
import com.project.exceptions.GenRunTimeException;
import com.project.exceptions.file.FileRunTimeException;
import com.project.utils.FileExceptionUtils;

/**
 * Used for connection pool for XML files
 * */
public class XmlFileConnection {

	private static XmlFileConnection _xmlCoupon = null;
	private static XmlFileConnection _xmlCompany = null;
	private static XmlFileConnection _xmlCustomer = null;
	private static XmlFileConnection _xmlInbox = null;
	private File _file;
	private boolean _availableConnection = true;
	
	static {
		try {
			_xmlCompany = new XmlFileConnection(XmlDB.COMPANY_XMLFILE);
			_xmlCustomer = new XmlFileConnection(XmlDB.CUSTOMER_XMLFILE);
			_xmlCoupon = new XmlFileConnection(XmlDB.COUPON_XMLFILE);
			_xmlInbox = new XmlFileConnection(XmlDB.INBOX_XMLFILE);
		} catch (FileRunTimeException e) {
			FileExceptionUtils.fileException(e);
		}
	}
	
	private XmlFileConnection( final String fileName ) throws FileRunTimeException {	
		try {
			_file = new File(fileName);
		} catch (NullPointerException e) {
			throw new FileRunTimeException(e, FileRunTimeException.NULLPOINTER);
		}
	}
	
	public static XmlFileConnection getXmlCompanyInstance() {
		return _xmlCompany;
	}
	
	public static XmlFileConnection getXmlCustomerInstance() {
		return _xmlCustomer;
	}
	
	public static XmlFileConnection getXmlCouponInstance() {
		return _xmlCoupon;
	}
		
	public static XmlFileConnection getXmlInboxInstance() {
		return _xmlInbox;
	}
	
	public static XmlFileConnection dispatcherInstance( String filename ) {
		if ( filename == null )
			return null;
		if ( filename.equalsIgnoreCase(XmlDB.COMPANY_XMLFILE) )
			return getXmlCompanyInstance();
		if ( filename.equalsIgnoreCase(XmlDB.CUSTOMER_XMLFILE) )
			return getXmlCustomerInstance();
		if ( filename.equalsIgnoreCase(XmlDB.COUPON_XMLFILE) )
			return getXmlCouponInstance();
		if ( filename.equalsIgnoreCase(XmlDB.INBOX_XMLFILE) )
			return getXmlInboxInstance();
		
		return null;
	}
	
	public synchronized File getConnection() throws GenRunTimeException {
		if ( ! _availableConnection )
			try {
				wait();
			} catch (IllegalMonitorStateException e) {
				throw new GenRunTimeException(e, GenRunTimeException.ILLEGAL_MON);
			} catch (InterruptedException e) {
				throw new GenRunTimeException(e, GenRunTimeException.INTERRUPTED);
			}
		
		_availableConnection = false;
		return _file;
	}
	
	public synchronized void returnConnection( File file ) throws FileRunTimeException {
//		try {
//			//writer.flush();
//		} catch (IOException e) {
//			throw new FileRunTimeException(e, FileRunTimeException.IO_ERR);
//		}
		_availableConnection = true;
		notifyAll();
	}
	
//	public synchronized void closeFileConnection() throws FileRunTimeException {
//		try {
//			_file.;
//		} catch (IOException e) {
//			throw new FileRunTimeException(e, FileRunTimeException.IO_ERR);
//		}
//	}
//	
//	public static void closeAllFilesConnections() throws FileRunTimeException {	
//		_xmlCompany.closeFileConnection();
//		_xmlCustomer.closeFileConnection();
//		_xmlCoupon.closeFileConnection();
//		_xmlInbox.closeFileConnection();
//	}
	

}
