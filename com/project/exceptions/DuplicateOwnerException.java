package com.project.exceptions;

public class DuplicateOwnerException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public DuplicateOwnerException( String msg ) {
		super(msg);
	}
	
	
}
