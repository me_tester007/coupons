package com.project.exceptions.sql;

import java.sql.SQLException;

public class InboxSqlException extends SqlRunTimeException {

	private static final long serialVersionUID = 1L;

	public InboxSqlException(String msg, int errorCode) {
		super(msg, errorCode);
	}
	
	public InboxSqlException( String msg ) {
		super(msg);
	}
	
	public InboxSqlException( SQLException e ) {
		super(e);
	}

	
	
}
