package com.project.exceptions.sql;

import java.sql.SQLException;

public class OwnerSqlException extends SqlRunTimeException {

	private static final long serialVersionUID = 1L;

	public OwnerSqlException(String msg, int errorCode) {
		super(msg, errorCode);
	}

	public OwnerSqlException( String msg ) {
		super(msg);
	}
	
	public OwnerSqlException( SQLException e ) {
		super(e);
	}
	


}
