package com.project.exceptions.sql;

import java.sql.SQLException;

public class CompanySqlException extends OwnerSqlException {

	private static final long serialVersionUID = 1L;

	public CompanySqlException(String msg, int errorCode) {
		super(msg, errorCode);
	}

	public CompanySqlException( String msg ) {
		super(msg);
	}
	
	public CompanySqlException( SQLException e ) {
		super(e);
	}
	

}
