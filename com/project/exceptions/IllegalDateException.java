package com.project.exceptions;

public class IllegalDateException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public IllegalDateException( String msg ) {
		super(msg);
	}

}
