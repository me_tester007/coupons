package com.project.exceptions;

public class IllegalPriceException extends RuntimeException {

	
	private static final long serialVersionUID = 1L;
	
	public IllegalPriceException( String msg ) {
		super(msg);
	}

}
