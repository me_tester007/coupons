package com.project.exceptions;

public class IllegalEmailException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public IllegalEmailException( String msg ) {
		super(msg);
	}
}
