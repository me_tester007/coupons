package com.project.exceptions;

public class GenRunTimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public static final int ILLEGAL_MON 		= 1;
	public static final int INTERRUPTED 		= 2;
	public static final int ILLEGAL_ARG 		= 3;
	public static final int CLASS_FOUND 		= 4;
	public static final int INSTANTIATION 		= 5;
	public static final int ILLEGALACCESS		= 6;
	
	private Exception _exception;
	private int _err;
	
	public GenRunTimeException( Exception e, int err ) {
		super(e.getMessage());
		_err = err;
		_exception = e;
	}
	
	public GenRunTimeException( String msg, int err ) {
		super(msg);
		_err = err;
	}
	
	public Exception getException() {
		return _exception;
	}
	
	public int getError() {
		return _err;
	}
	
	public String getExceptionMessage() {
		return _exception.getMessage();
	}
	
}
