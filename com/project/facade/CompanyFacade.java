package com.project.facade;

import java.util.Collection;

import com.project.beans.Company;
import com.project.beans.Coupon;
import com.project.beans.CouponType;
import com.project.beans.Inbox;
import com.project.constants.SqlDB;
import com.project.dao.CompanyDao;
import com.project.dao.CouponDao;
import com.project.daofile.LogLevel;
import com.project.daofile.LogType;
import com.project.exceptions.CouponNotExistException;
import com.project.exceptions.sql.CouponSqlException;
import com.project.exceptions.sql.InboxSqlException;
import com.project.exceptions.sql.OwnerSqlException;
import com.project.logs.Logger;
import com.project.utils.ExceptionUtils;
import com.project.utils.FactoryDao;
import com.project.utils.InboxUtils;
import com.project.utils.SqlExceptionUtils;

/**
 * This Class represents the Company object with all basic operations<br/>
 * that Company can perfrom on this server
 * */
public class CompanyFacade implements CouponClientFacade {
	
	private int _companyID;
	private String _companyName;
	private CouponDao _couponDao;
	private CompanyDao _companyDao;
	
	public CompanyFacade() {
		_couponDao = FactoryDao.getCouponDao();
		_companyDao = FactoryDao.getCompanyDao();;
	}	

	/*
	* creates coupon object in DB table and logs the operation following by
	* sending notification message to the inbox of this company
	*/
	public void createCoupon( Coupon coupon ) {
		if ( coupon == null )
			return;
		
		Inbox inbox = new Inbox(InboxUtils.getNextID(), "Congrats "+ _companyName +". You have successfully created Coupon " 
				+ coupon.getID() + ":" + coupon.getTitle());
		try {
			_couponDao.createCoupon(coupon);
			_couponDao.assignCouponToOwner(_companyID, coupon.getID(), SqlDB.COMPANY_COUPON_TABLE);
			Logger.inboxLog(inbox);
			Logger.updateOwnerInbox(_companyID, inbox.getID(), SqlDB.COMPANY_INBOX_TABLE);
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
			return;
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
		}
		String msg = "Coupon " + coupon.getTitle() + " was successfuly created by " +_companyName + " company";
		Logger.log(msg, LogLevel.INFO);
		Logger.log(msg, LogType.COMPANY, LogLevel.INFO);
	}
	
	/*
	 * removes the specific coupon from DB table
	 * logs the opration
	 * */
	public void removeCoupon( Coupon coupon ) {
		if ( coupon == null )
			return;
		
		Inbox inbox = new Inbox(InboxUtils.getNextID(), "You have removed Coupon "
				+ coupon.getID() + ":" + coupon.getTitle());
		try {  // If we did not find any owner / if coupon does not exist we throw exception
			if ( _couponDao.getCouponOwnerID(coupon, SqlDB.COMPANY_COUPON_TABLE) < 0  )
				throw new CouponNotExistException(coupon);
				
			Logger.inboxLog(inbox);
			Logger.updateOwnerInbox(_companyID, inbox.getID(), SqlDB.COMPANY_INBOX_TABLE);
			_couponDao.removeCouponFromOwner(coupon.getID(), SqlDB.COMPANY_COUPON_TABLE);
			//if there is NO customer possessing that coupon we completely remove it 
			//from coupon table
			if ( _couponDao.getCouponOwnerID(coupon, SqlDB.CUSTOMER_COUPON_TABLE) == -1 )
				_couponDao.removeCoupon(coupon);
			//otherwise We just Nullifying the ammount of the coupon so nobody can aquire it
			//but we still can have reference to it from other customer join table
			// and it eventually will be removed when expired
			else {
				coupon.setAmount(0);
				_couponDao.updateCoupon(coupon);
			}
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
			return;
		} catch (CouponNotExistException e) {
			ExceptionUtils.couponNotExist(e);
			return;
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
		}
		String msg = "Coupon " + coupon.getTitle() + " was removed by " +_companyName + " company";
		Logger.log(msg, LogLevel.INFO);
		Logger.log(msg, LogType.COMPANY, LogLevel.INFO);
	}
	
	/*
	 * updates info of specific coupon object, logs the operation
	 * following by sending update message to the owner
	 * */
	public void updateCoupon( Coupon coupon ) {
		if ( coupon == null )
			return;
		
		Inbox inbox = new Inbox(InboxUtils.getNextID(), "You have updated the coupon " 
				+ coupon.getID() + ":" + coupon.getTitle());
		try {
			_couponDao.updateCoupon(coupon);
			Logger.updateOwnerInbox(_companyID, inbox.getID(), SqlDB.COMPANY_INBOX_TABLE);
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
			return;
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
		}
		String msg = "Coupon " + coupon.getTitle() + " was updated by " +_companyName + " company";
		Logger.log(msg, LogLevel.INFO);
		Logger.log(msg, LogType.COMPANY, LogLevel.INFO);
	}
	
	// retrieves the sepcific coupon by its ID
	public Coupon getCoupon( int id ) {
		if ( id <= 0 )
			return null;
		
		Coupon coupon = null;
		try {
			coupon = _couponDao.getCoupon(id); 
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
		}
		return coupon;
	}
	
	// retirieves all available coupons for this specific company
	public Collection<Coupon> getAllCoupons() {
		
		Collection<Coupon> coupons = null;
		try {
			coupons = _companyDao.getCoupons(_companyID);
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
		}
		return coupons;
	}
	
	// retrieves all inbox messages for this specific company
	public Collection<Inbox> getAllInboxMessages() {
		
		Collection<Inbox> inboxes = null;
		try {
			inboxes = _companyDao.getInboxMessages(_companyID);
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
		}
		return inboxes;
	}

	// retrieves all avaiable coupons by Type for this specific company
	public Collection<Coupon> getCouponByType( CouponType type ) {
		if ( type == null || type == CouponType.UNDEFINED )
			return null;
		
		Collection<Coupon> coupons = getAllCoupons();
		if ( coupons == null )
			return null;
		for (Coupon coupon : coupons)
			if ( coupon.getType() != type )
				coupons.remove(coupon);
	
		return coupons;
	}
		
	/* 
	 * Test the Company credentials in case of successful authorization return the object instance
	 * log operations
	 * */
	@Override
	public CouponClientFacade login( String name, String password, ClientType clientType ) {
		if ( name == null || password == null || clientType != ClientType.COMPANY )
			return null;
		
		Company company =  null;
		try {
			if ( !_companyDao.login(name, password) ) {
				Logger.log("Company name:" + name + "password:" + password + " unsuccessful login attempt", LogLevel.INFO);
				return null;
			}
			company = _companyDao.getCompany(name);
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.companyException(e);
			return null;
		}
	
		_companyName = company.getName();
		_companyID = company.getID();
		String msg = "Company " + _companyName + " successfully logged in";
		Logger.log(msg, LogLevel.INFO);
		Logger.log(msg, LogType.COMPANY, LogLevel.INFO);
		return this;
	}

}
