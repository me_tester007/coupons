package com.project.facade;

import java.util.Collection;

import com.project.beans.Coupon;
import com.project.beans.CouponType;
import com.project.beans.Customer;
import com.project.beans.Inbox;
import com.project.constants.SqlDB;
import com.project.dao.CouponDao;
import com.project.dao.CustomerDao;
import com.project.daofile.LogLevel;
import com.project.daofile.LogType;
import com.project.exceptions.NegativeAmountException;
import com.project.exceptions.sql.CouponSqlException;
import com.project.exceptions.sql.CustomerSqlException;
import com.project.exceptions.sql.InboxSqlException;
import com.project.logs.Logger;
import com.project.utils.ExceptionUtils;
import com.project.utils.FactoryDao;
import com.project.utils.InboxUtils;
import com.project.utils.SqlExceptionUtils;

/**
 * This Class represents the Customer object with all basic operations<br/>
 * that Customer can perfrom on this server
 * */
public class CustomerFacade implements CouponClientFacade {

	private int _customerID;
	private String _customerName;
	private CouponDao _couponDao;
	private CustomerDao _customerDao;
	
	public CustomerFacade() {
		_couponDao = FactoryDao.getCouponDao();
		_customerDao = FactoryDao.getCustomerDao();
	}
	
	/*
	* purchase / creates coupon entry in DB JOIN table and logs the operation following by
	* sending notification message to the inbox of this customer
	*/
	public void purchaseCoupon( Coupon coupon ) {
		if ( coupon == null )
			return;
		
		//First try if decreasing the Amount of Coupon is possible
		//Check If we actually can BUY this coupon
		try {
			coupon.setAmount(coupon.getAmount() - 1);
		} catch (NegativeAmountException e) {
			ExceptionUtils.absentInStock(e);
			return;
		}
		
		Inbox inbox = new Inbox(InboxUtils.getNextID(), "Congrats " + _customerName +
				". You have acquired Coupon " + coupon.getID() + ":" + coupon.getTitle());
		try {
			_couponDao.assignCouponToOwner(_customerID, coupon.getID(), SqlDB.CUSTOMER_COUPON_TABLE);
			_couponDao.updateCoupon(coupon); //update the new amount
			Logger.inboxLog(inbox);
			Logger.updateOwnerInbox(_customerID, inbox.getID(), SqlDB.CUSTOMER_INBOX_TABLE);
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
			return;
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
		}
		String msg = "Coupon " + coupon.getID() + ":" + coupon.getTitle() 
		+ " was purchased by customer " + _customerName;
		Logger.log(msg, LogLevel.INFO);
		Logger.log(msg, LogType.CUSTOMER, LogLevel.INFO);
	}
	
	//retrieves all pruchased coupons from DB for this customer
	public Collection<Coupon> getAllPurchasedCoupons() {
		
		Collection<Coupon> coupons = null;
		try {
			coupons = _customerDao.getCoupons(_customerID);
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
		}
		return coupons;
	}
	
	//retrieves all inbox messages from DB for this customer
	public Collection<Inbox> getInboxMessages() {
		
		Collection<Inbox> inboxes = null;
		try {
			inboxes = _customerDao.getInboxMessages(_customerID);
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
		}
		return inboxes;
	}
	
	//retrieves all pruchased coupons by TYPE from DB for this customer
	public Collection<Coupon> getAllPurchasedCouponsByType( CouponType type ) {
		Collection<Coupon> coupons = getAllPurchasedCoupons();
		if ( coupons == null )
			return null;
		
		for ( Coupon coupon : coupons )
			if ( coupon.getType() != type )
				coupons.remove(coupon);
	
		return coupons;
	}
	
	//retrieves all pruchased coupons by PRICE from DB for this customer
	public Collection<Coupon> getAllPurchasedCouponsByPrice( double price ) {
		Collection<Coupon> coupons = getAllPurchasedCoupons();
		if ( coupons == null )
			return null;
		
		for ( Coupon coupon : coupons )
			if ( coupon.getPrice() != price )
				coupons.remove(coupon);
	
		return coupons;
	}
	
	/* 
	 * Test the Customer credentials in case of successful authorization return the object instance
	 * log operations
	 * */
	@Override
	public CouponClientFacade login( String name, String password, ClientType clientType ) {
		if ( name == null || password == null || clientType != ClientType.CUSTOMER )
			return null;
	
		Customer customer = null;
		try {
			if ( ! _customerDao.login(name, password) ) {
				Logger.log("Customer name:" + name + " password:" + password + " unsuccessful login attempt", LogLevel.INFO);
				return null;
			}
			customer = _customerDao.getCustomer(name);
		} catch (CustomerSqlException e) {
			SqlExceptionUtils.customerException(e);
			return null;
		}
		_customerName = customer.getName();
		_customerID = customer.getID();
		String msg = "Customer " + _customerName + " successfully logged in";
		Logger.log(msg, LogLevel.INFO);
		Logger.log(msg, LogType.CUSTOMER, LogLevel.INFO);
		return this;
	}

}
