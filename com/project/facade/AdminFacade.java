package com.project.facade;

import java.util.Collection;
import com.project.beans.Company;
import com.project.beans.Customer;
import com.project.beans.Inbox;
import com.project.constants.ServerAdmin;
import com.project.constants.SqlDB;
import com.project.dao.CompanyDao;
import com.project.dao.CouponDao;
import com.project.dao.CustomerDao;
import com.project.dao.InboxDao;
import com.project.daofile.LogLevel;
import com.project.exceptions.sql.CouponSqlException;
import com.project.exceptions.sql.InboxSqlException;
import com.project.exceptions.sql.OwnerSqlException;
import com.project.logs.Logger;
import com.project.utils.FactoryDao;
import com.project.utils.InboxUtils;
import com.project.utils.SqlExceptionUtils;

/**
 * This Class represents the Administrator object with all basic operations<br/>
 * that Administartor can perfrom on this server
 * */
public class AdminFacade implements CouponClientFacade {
	
	private CompanyDao _companyDao;
	private CustomerDao _customerDao;
	private CouponDao _couponDao;
	private InboxDao _inboxDao;
	
	//initialized four objects on startup for further usage
	public AdminFacade(  ) {
		_companyDao = FactoryDao.getCompanyDao();
		_customerDao = FactoryDao.getCustomerDao();
		_couponDao = FactoryDao.getCouponDao();
		_inboxDao = FactoryDao.getInboxDao();
	}
	
	/*
	* creates company object in DB table and logs the operation following by
	* sending notification message to the inbox of this created company
	*/
	public void createCompany( Company company ) {
		if ( company == null )
			return;
		
		Inbox inbox = new Inbox( InboxUtils.getNextID(), "Hi " + company.getName()
		+ " Welcome to Coupon Management System!");
		try {
			_companyDao.createCompany(company);
			//create single inbox message in the DB table
			Logger.inboxLog(inbox); 
			//and assign this message to the company just created
			Logger.updateOwnerInbox(company.getID(), inbox.getID(), SqlDB.COMPANY_INBOX_TABLE);
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.companyException(e);
			return;
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
		}
		Logger.log("Company " + company.getName() + " was successfully created", LogLevel.INFO);
	}
	
	/*
	 * removes the specific company from DB table along with all its belonging coupons
	 * and inbox messages
	 * logs the opration
	 * */
	public void removeCompany( Company company ) {
		if ( company == null )
			return;

		try {
			// Checks if the company has coupons/inbox messages in the join tables and safely deletes them too
			_couponDao.removeCouponOwner(company.getID(), SqlDB.COMPID_COLUMN, SqlDB.COMPANY_COUPON_TABLE);
			_inboxDao.removeInboxOwner(company.getID(), SqlDB.COMPID_COLUMN, SqlDB.COMPANY_INBOX_TABLE);
			_companyDao.removeCompany(company);
		} catch ( CouponSqlException e ) {
			SqlExceptionUtils.couponException(e);
			return;
		} catch ( OwnerSqlException e ) {
			SqlExceptionUtils.companyException(e);
			return;
		} catch ( InboxSqlException e ) {
			SqlExceptionUtils.inboxException(e);
			return;
		}
		Logger.log("Company id:" + company.getID() + " name:" + company.getName() + " was successfully removed", LogLevel.INFO);
	}

	/*
	 * updates info of specific company object, logs the operation
	 * following by sending update message to the modified owner
	 * */
	public void updateCompany( Company company ) {
		if ( company == null )
			return;
		
		Inbox inbox = new Inbox(InboxUtils.getNextID(), "Dear " + company.getName() + " your info was updated");
		try {
			_companyDao.updateCompany(company);
			Logger.inboxLog(inbox);
			Logger.updateOwnerInbox(company.getID(), inbox.getID(), SqlDB.COMPANY_INBOX_TABLE);
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.companyException(e);
			return;
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
			return;
		}
		Logger.log("Company " + company.getName() + "info was updated", LogLevel.INFO);
	}
	
	//retrieves specific company from db table by its ID
	public Company getCompany( int id ) {
		if ( id <= 0 )
			return null;
		
		Company company = null;
		try {
			company = _companyDao.getCompany(id);
			company.setCoupons(_companyDao.getCoupons(id));
			company.setInboxMessages(_companyDao.getInboxMessages(id));
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.companyException(e);
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
		}
		return company;
	}

	// Retrieves all companies from DB table
	public Collection<Company> getAllCompanies() {
		Collection<Company> companies = null;
		try {
			companies = _companyDao.getAllCompanies();
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.companyException(e);
		}
		getCompaniesCoupons(companies);
		getCompaniesInboxes(companies);
		return companies;
	}

	/*
	* creates customer object in DB table and logs the operation following by
	* sending notification message to the inbox of this created customer
	*/
	public void createCustomer( Customer customer ) {
		if ( customer == null )
			return;
		
		Inbox inbox = new Inbox( InboxUtils.getNextID(), "Hi " + customer.getName() 
		+ " Welcome to Coupon Management System!");
		try {
			_customerDao.createCustomer(customer);
			Logger.inboxLog(inbox);
			Logger.updateOwnerInbox(customer.getID(), inbox.getID(), SqlDB.CUSTOMER_INBOX_TABLE);
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.customerException(e);
			return;
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
			return;
		}
		Logger.log("Customer " + customer.getName() + " was created", LogLevel.INFO);
	}
	
	/*
	 * removes the specific company from DB table along with all its belonging coupons
	 * and inbox messages
	 * logs the opration
	 * */
	public void removeCustomer( Customer customer ) {
		if ( customer == null )
			return;

		try {
			// Checks if the customer has coupons/inbox messages in the join tables and safely deletes tjem too
			_couponDao.removeCouponOwner(customer.getID(), SqlDB.CUSTID_COLUMN, SqlDB.CUSTOMER_COUPON_TABLE);
			_inboxDao.removeInboxOwner(customer.getID(), SqlDB.CUSTID_COLUMN, SqlDB.CUSTOMER_INBOX_TABLE);
			_customerDao.removeCustomer(customer);
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
			return;
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
			return;
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.customerException(e);
			return;
		}
		Logger.log("Customer " + customer.getName() + " was successfully removed", LogLevel.INFO);
	}
	
	/*
	 * updates info of specific customer object, logs the operation
	 * following by sending update message to the modified owner
	 * */
	public void updateCustomer( Customer customer ) {
		if ( customer == null )
			return;
		
		Inbox inbox = new Inbox(InboxUtils.getNextID(), "Dear " + customer.getName() + " your info was updated");
		try {
			_customerDao.updateCustomer(customer);
			Logger.inboxLog(inbox);
			Logger.updateOwnerInbox(customer.getID(), inbox.getID(), SqlDB.CUSTOMER_INBOX_TABLE);
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.customerException(e);
			return;
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
			return;
		}
		Logger.log("Customer " + customer.getName() + " was successfully updated", LogLevel.INFO);
	}

	//retrieves specific customer from db table by its ID
	public Customer getCustomer( int id ) {
		if ( id <= 0 )
			return null;
		
		Customer customer = null;
		try {
			customer = _customerDao.getCustomer(id);
			customer.setCoupons(_customerDao.getCoupons(id));
			customer.setInboxMessages(_customerDao.getInboxMessages(id));
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.customerException(e);
		} catch (CouponSqlException e) {
			SqlExceptionUtils.couponException(e);
		} catch (InboxSqlException e) {
			SqlExceptionUtils.inboxException(e);
		}
		return customer;
	}

	// Retrieves all customer from DB table
	public Collection<Customer> getAllCustomers() {
		Collection<Customer> customers = null;
		try {
			customers = _customerDao.getAllCustomers();
		} catch (OwnerSqlException e) {
			SqlExceptionUtils.customerException(e);
		}
		getCustomersCoupons(customers);
		getCustomersInboxes(customers);
		return customers;
	}
	
	/* 
	 * Test the Administrator credentials in case of successful authorization return the object instance
	 * log operations
	 * */
	@Override
	public CouponClientFacade login( String name, String password, ClientType clientType ) {
		if ( name == null || password == null || clientType != ClientType.ADMIN )
			return null;

		if ( ! name.equals(ServerAdmin.USERNAME) || ! password.equals(ServerAdmin.PASSWORD) ) {
			Logger.log("Unsuccessful admin loggin attempt", LogLevel.INFO);
			return null;
		}
		
		Logger.log("Admin " + name + " successfully logged in", LogLevel.INFO);
		return this;
	}

	private void getCompaniesCoupons( Collection<Company> companies ) {
		for (Company company : companies) {
			try {
				company.setCoupons(_companyDao.getCoupons(company.getID()));
			} catch (CouponSqlException e) {
				SqlExceptionUtils.couponException(e);;
			}
		}
	}

	private void getCompaniesInboxes( Collection<Company> companies ){
		for (Company company : companies) {
			try {
				company.setInboxMessages(_companyDao.getInboxMessages(company.getID()));
			} catch (InboxSqlException e) {
				SqlExceptionUtils.inboxException(e);
			}
		}
	}
	
	private void getCustomersCoupons( Collection<Customer> customers ) {
		for (Customer customer : customers) {
			try {
				customer.setCoupons(_customerDao.getCoupons(customer.getID()));
			} catch (CouponSqlException e) {
				SqlExceptionUtils.couponException(e);;
			}
		}
	}

	private void getCustomersInboxes( Collection<Customer> customers ){
		for (Customer customer : customers) {
			try {
				customer.setInboxMessages(_customerDao.getInboxMessages(customer.getID()));
			} catch (InboxSqlException e) {
				SqlExceptionUtils.inboxException(e);
			}
		}
	}

}
