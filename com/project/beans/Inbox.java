package com.project.beans;

/**
 * This Java Bean class represents any specific Inbox<br>that would acquire Coupons
* for different purposes<p>
* Its attributes are being saved/read into/from  DB
*/
public class Inbox implements Accessory {

	private int _id;
	private String _msg;
	
	public Inbox(){}
	
	public Inbox( int id, String msg ) {
		setID(id);
		setMessage(msg);
	}
	
	//--- Getters
	@Override
	public int getID() {
		return _id;
	}
	
	public String getMessage() {
		return _msg;
	}
	
	//--- Setters
	public void setID( int id ) {
		if ( id <= 0 )
			return;
		_id = id;
	}
	
	public void setMessage( String msg ) {
		if ( msg == null )
			return;
		_msg = msg;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if ( obj == null ) return false;
		if ( obj == this ) return true;
		if ( !(obj instanceof Inbox) ) return false;
		return _id == ((Inbox)obj)._id;	
	}

	@Override
	public String toString() {
		return "InboxMessage [_id=" + _id + ", _msg=" + _msg + "]";
	}
	
}
