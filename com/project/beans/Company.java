package com.project.beans;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import com.project.exceptions.IllegalCompanyIdException;
import com.project.exceptions.IllegalCompanyNameException;
import com.project.exceptions.IllegalEmailException;
import com.project.exceptions.IllegalPasswordException;
import com.project.utils.EmailUtils;
import com.project.utils.PasswordUtils;

/**
 * This Java Bean class represents any specific Company from company DB table<br>
 * that would make/produce Coupons for different purposes<p>
 * Its attributes are being saved/read into/from MySQL DB (company table)
 * */
public class Company implements Owner {

	private int _id;
	private String _companyName;
	private String _password;
	private String _email;
	private Collection<Coupon> _coupons;
	private Collection<Inbox> _inbox;
	
	public Company() {}
	
	public Company( int id, String compName, String pwd,
			String email, Collection<Coupon> coupons, Collection<Inbox> inbox ) {
		setID(id);
		setName(compName);
		setPassword(pwd);
		setEmail(email);
		setCoupons(coupons);
		setInboxMessages(inbox);
	}
	
	/*
	 *  			GETTERS
	 * */
	@Override
	public int getID() {
		return _id;
	}
	
	@Override
	public String getName() {
		return _companyName;
	}
	
	@Override
	public String getPassword() {
		return _password;
	}
	
	@Override
	public String getEmail() {
		return _email;
	}
	
	@Override
	public Collection<Coupon> getCoupons() {
		return Collections.unmodifiableCollection(_coupons);
	}
	
	@Override
	public Collection<Inbox> getInboxMessages() {
		return Collections.unmodifiableCollection(_inbox);
	}
	
	/*
	 * 			SETTERS
	 * */
	@Override
	public void setID( int id ) throws IllegalCompanyIdException {
		if ( id <= 0 )
			throw new IllegalCompanyIdException("Company ID is illegal");
		_id = id;
	}
	
	@Override
	public void setName( String compName ) throws IllegalCompanyNameException {
		if ( compName == null )
			throw new IllegalCompanyNameException("Company name is null");
		_companyName = compName;
	}
	
	@Override
	public void setPassword( String pwd ) throws IllegalPasswordException {
		if ( ! PasswordUtils.isPwdLegal(pwd) )
			throw new IllegalPasswordException("Password does not match the minimum policy requirement");
		_password = pwd;
	}
	
	@Override
	public void setEmail( String email ) throws IllegalEmailException {
		if ( ! EmailUtils.isEmailAddressLegal(email) )
			throw new IllegalEmailException("Invalid email recieved");
		_email = email;
	}
	
	@Override
	public void setCoupons( Collection<Coupon> coupons ) {
		if ( coupons == null ) {
			if ( _coupons == null )
				_coupons = new HashSet<>();
		} else {
			_coupons = coupons;
		}
	}
	
	@Override
	public void setInboxMessages( Collection<Inbox> inbox ) {
		if ( inbox == null ) {
			if ( _inbox == null )
				_inbox = new HashSet<>();
		} else {
			_inbox = inbox;
		}
	}
	
	
	@Override
	public boolean equals( Object obj ) {
		if ( obj == null ) return false;
		if ( obj == this ) return true;
		if ( ! (obj instanceof Company) ) 
			return false;
		
		return _id == ((Company)obj)._id && 
				_companyName.equals(((Company)obj)._companyName);
	}

	@Override
	public String toString() {
		return "Company [_id=" + _id + ", _compName=" + _companyName + ", _password=" + _password + ", _email=" + _email
				+ ", _coupons=" + _coupons + ", _inbox=" + _inbox + "]";
	}
		
}
