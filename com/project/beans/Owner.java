package com.project.beans;

import java.util.Collection;

/**
 * Interface represents either Company or Customer object
 * */
public interface Owner {
	
	public int getID();
	public String getName();
	public String getPassword();
	public String getEmail();
	public Collection<Coupon> getCoupons();
	public Collection<Inbox> getInboxMessages();

	public void setName( String name );
	public void setID( int id );
	public void setPassword( String pwd );
	public void setEmail( String email);
	public void setCoupons( Collection<Coupon> coupons );
	public void setInboxMessages( Collection<Inbox> inbox );
	
}
