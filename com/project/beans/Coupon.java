package com.project.beans;


import java.sql.Date;
import java.util.Comparator;

import com.project.exceptions.NegativeAmountException;
import com.project.exceptions.IllegalCouponIdException;
import com.project.exceptions.IllegalDateException;
import com.project.exceptions.IllegalPriceException;

/**
 * 
 *  This Java Bean class represents any specific Coupon<br>that would acquire Coupons
 * for different purposes<p>
 * Its attributes are being saved/read into/from  DB
 * 
 * */
public class Coupon implements Comparable<Coupon>, Accessory {

	private int _id;
	private String _title;
	private Date _startDate;
	private Date _endDate;
	private int _amount;
	private CouponType _type;
	private String _message;
	private double _price;
	private String _image;
	
	public Coupon() {
		_type = CouponType.UNDEFINED;
	}
	
	public Coupon( int id, String title, Date start, Date end, int amount,
			CouponType type, String msg, double price, String image ) {
		setID(id);
		setTitle(title);
		setStartDate(start);
		setEndDate(end);
		setAmount(amount);
		setType(type);
		setMessage(msg);
		setPrice(price);
		setImage(image);
	}
	
	/*
	 * 		GETTERS
	 * */
	@Override
	public int getID() {
		return _id;
	}
	
	public String getTitle() {
		return _title;
	}
	
	public Date getStartDate() {
		return _startDate;
	}
	
	public Date getEndDate() {
		return _endDate;
	}
	
	public int getAmount() {
		return _amount;
	}
	
	public CouponType getType() {
		return _type;
	}
	
	public String getMessage() {
		return _message;
	}
	
	public double getPrice() {
		return _price;
	}
	
	public String getImage() {
		return _image;
	}
	
	/*
	 * 		SETTERS
	 * */

	public void setID( int id ) throws IllegalCouponIdException {
		if ( id <=0 )
			throw new IllegalCouponIdException("Coupon ID is illegal");
		_id = id;
	}
	
	public void setTitle( String title ) {
		if ( title == null ) {
			if( _title == null )
				_title = "Undefined";
		} else {
			_title = title;
		}
	}
	
	public void setStartDate( Date startDate ) throws IllegalDateException {
		if ( startDate == null )
			throw new IllegalDateException("startDate variable is null");
		_startDate = startDate;
	}

	public void setEndDate( Date endDate ) throws IllegalDateException {
		if ( endDate == null )
			throw new IllegalDateException("endDate variable is null");
		_endDate = endDate;
	}
	
	public void setAmount( int amount ) throws NegativeAmountException {
		if ( amount < 0 )
			throw new NegativeAmountException("Negative amount value");
		_amount = amount;
	}
	
	public void setType( CouponType type ) {
		if ( type == null ) {
			if ( _type == null )
				_type = CouponType.UNDEFINED;
		} else {
			_type = type;
		}
	}
	
	public void setMessage( String msg ) {
		if ( msg == null ) {
			if ( _message == null )
				_message = "Uninitialized";
		} else {
			_message = msg;
		}
	}
	
	public void setPrice( double price ) throws IllegalPriceException {
		if ( price < 0.0 )
			throw new IllegalPriceException("Price is illegal negative value");
		_price = price;
	}
	
	public void setImage( String image ) {
		if ( image == null ) {
			if ( _image == null )
				_image = "Path not set";
		} else {
			_image = image;
		}
	}
	
	@Override
	public boolean equals( Object obj ) {
		if ( obj == null ) return false;
		if ( obj == this ) return true;
		if ( ! (obj instanceof Coupon) ) 
			return false;
		
		return _id == ((Coupon)obj)._id &&
				_title.equals(((Coupon)obj).getTitle());
	}

	@Override
	public String toString() {
		return "Coupon [_id=" + _id + ", _title=" + _title + ", _startDate=" + _startDate + ", _endDate=" + _endDate
				+ ", _amount=" + _amount + ", _type=" + _type + ", _message=" + _message + ", _price=" + _price
				+ ", _image=" + _image + "]";
	}

	@Override
	public int compareTo( Coupon o ) {
		return _title.compareTo(o.getTitle());
	}
}

class SortCouponsById implements Comparator<Coupon> {

	@Override
	public int compare( Coupon o1, Coupon o2 ) {
		return o1.getID() - o2.getID();
	}
	
}
