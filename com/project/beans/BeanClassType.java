package com.project.beans;

/**
 * Class needed in order to dynamically create objects in case any condition matches
 * usually is used in condition statements
 * */
public class BeanClassType {

	public static final String COMPANY = "com.project.beans.Company";
	public static final String CUSTOMER = "com.project.beans.Customer";
	public static final String COUPON = "com.project.beans.Coupon";
	public static final String INBOX = "com.project.beans.Inbox";
	
}
