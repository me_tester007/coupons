package com.project.beans;

/**
 * Accessory interface represnts either Coupon or Inbox object
 * */
public interface Accessory {

	public int getID();
}
