package com.project;

import java.util.Properties;

import com.project.daofile.LogFileConnection;
import com.project.daofile.LogLevel;
import com.project.exceptions.file.FileRunTimeException;
import com.project.facade.AdminFacade;
import com.project.facade.ClientType;
import com.project.facade.CompanyFacade;
import com.project.facade.CouponClientFacade;
import com.project.facade.CustomerFacade;
import com.project.jdbc.ConnectionPool;
import com.project.logs.Logger;
import com.project.tasks.DayliCouponExpirationTask;
import com.project.tasks.ServerParamsInitializer;
import com.project.utils.FactoryDao;
import com.project.utils.FileExceptionUtils;
import com.project.utils.PropertiesValidator;

/**
 * Main Coupon System class. Singleton that will be loaded first at startup
 * */
public class CouponSystem {
	// Singleton
	private static final CouponSystem _cs = new CouponSystem();
	private Properties _properties = null;
	private Thread _couponExpirationThread = null;
	private DayliCouponExpirationTask _task;

	private CouponSystem() {
		_initializeSettings();
	}

	/*
	 *  Initializes the system right after starting
	 *  determines DB type and other parmeters read from properties files
	*/
	private void _initializeSettings() throws FileRunTimeException {
		/*gets all initial parameters  from property file
		 * IF DOES NOT EXIST - it is automatically being created with default parameters
		 * and database set to XML DB
		 */
		try {
			_properties = PropertiesValidator.getInitialParameters();
		} catch (FileRunTimeException e) {
			FileExceptionUtils.fileException(e);
		}
		if (_properties == null){
			Logger.log("Cannot start application. DB Properties file parse error", LogLevel.FATAL);
			shutdown();
		}

		_initAllSystemParameters(_properties);

		_task = new DayliCouponExpirationTask();
		_couponExpirationThread = new Thread(_task, "Coupon Expiration Task");
		//couponExpiratioinThread.setDaemon(true);
		_couponExpirationThread.start();
	}
	
	/**
	 * Initialzes each component/module of the system if required
	 * */
	private void _initAllSystemParameters( Properties properties ) {
		ServerParamsInitializer.initServerAdminCredentials(_properties);
		ServerParamsInitializer.initLogParameters(_properties);
		ServerParamsInitializer.initSqlParameters(_properties);
		ServerParamsInitializer.initXmlParameters(_properties);
		FactoryDao.initFactoryDao(_properties);
		
		PropertiesValidator.closePropertyFile();
	}

	
	public static CouponSystem getInstance(){
		return _cs;
	}

	
	/**
	 * Login function creates and returns relevant facade object
	 * */
	public CouponClientFacade login( String name, String pwd, ClientType type ) {
		if ( name == null || pwd == null || type == null || type == ClientType.UNDEFINED )
			return null;

		switch(type) {

		case ADMIN:
			return new AdminFacade().login(name, pwd, type);

		case COMPANY:
			return new CompanyFacade().login(name, pwd, type);

		case CUSTOMER:
			return new CustomerFacade().login(name, pwd, type);

		case UNDEFINED:
			break;
		}
		return null;
	}

	
	/**
	 * Shutdown entire system
	 * */
	public void shutdown() {
		if ( _couponExpirationThread.isAlive() )
			_task.stopTask();

		LogFileConnection.closeAllFilesConnections();
		if ( ConnectionPool.getInstance() != null )
			ConnectionPool.getInstance().closeAllConnections();
		System.exit(0);
	}

}
