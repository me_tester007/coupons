package com.project;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.Collection;

import com.project.beans.Company;
import com.project.beans.Coupon;
import com.project.beans.CouponType;
import com.project.beans.Customer;
import com.project.constants.FileNames;
import com.project.constants.ServerAdmin;
import com.project.constants.SqlDB;
import com.project.constants.XmlDB;
import com.project.dao.DaoSchemaHandler;
import com.project.daosql.SchemaHandlerDBDao;
import com.project.facade.AdminFacade;
import com.project.facade.ClientType;
import com.project.facade.CompanyFacade;
import com.project.facade.CouponClientFacade;
import com.project.facade.CustomerFacade;
import com.project.utils.CompanyUtils;
import com.project.utils.CouponUtils;
import com.project.utils.CustomerUtils;
import com.project.utils.FactoryDao;

/*
 * */

public class TestMain {
	
	public static final int IBM 	= 0;
	public static final int INTEL 	= 1;
	public static final int GOOG 	= 2;
	public static final int FBOOK 	= 3;
	public static final int HP 		= 4;
	
	public static final int YANIV 	= 0;
	public static final int MOSHE 	= 1;
	public static final int ADAM 	= 2;
	public static final int DAVID 	= 3;
	public static final int IZIK 	= 4;
	
	private static CouponSystem CS = null;
	private static AdminFacade adminFacade = null;
	private static CompanyFacade[] compFacades = new CompanyFacade[10];
	private static CustomerFacade[] custFacades = new CustomerFacade[10];
	private static Company[] companies = new Company[10];
	private static Customer[] customers = new Customer[10];
	private static Coupon[] coupons = new Coupon[20];

	public static void printCompanyCoupons() {
		Collection<Coupon> ibmCoupons = compFacades[IBM].getAllCoupons();
		Collection<Coupon> intelCoupos = compFacades[INTEL].getAllCoupons();
		System.err.println("\nIBM related coupons");
		for (Coupon coupon : ibmCoupons) {
			
			System.out.println(coupon);
			_wait(1);
		}
		System.err.println("\nINTEL related coupons");
		for (Coupon coupon : intelCoupos) {
			System.out.println(coupon);
			_wait(1);
		}
	}
	
	public static void printCustomerCoupons() {
		Collection<Coupon> yanivCoupons = custFacades[YANIV].getAllPurchasedCoupons();
		Collection<Coupon> mosheCoupos = custFacades[MOSHE].getAllPurchasedCoupons();
		System.err.println("Coupons bougth by Yaniv");
		for (Coupon coupon : yanivCoupons) {
			System.out.println(coupon);
			_wait(1);
		}
		System.err.println("\nCoupons bougth by Moshe");
		for (Coupon coupon : mosheCoupos) {
			System.out.println(coupon);
			_wait(1);
		}
	}
	
	
	public static void printCompanies() {
		
		Collection<Company> companiess = adminFacade.getAllCompanies();
		for (Company company : companiess){
			System.out.println(company);
			_wait(1);
		}
			
	}
	
	public static void printCustomers( ) {
		Collection<Customer> customers = adminFacade.getAllCustomers();
		for (Customer customer : customers) {
			System.out.println(customer);
			_wait(1);
		}
	}
	
	public static void _wait( int sec ) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			System.err.println("_wait() method interrupted");
		}
	}
	
	public static void prugeXMLFiles() {
		for (int i = 0; i < XmlDB.XML_FILES.length; i++) {
			File file = new File(XmlDB.XML_FILES[i]);
			if ( file.exists() ) {
				file.delete();
				try {
					file.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public	static void purgeDatabase() {
		DaoSchemaHandler schemah = new SchemaHandlerDBDao();
		schemah.purgeAllTable();
	}
	
	public static void couponSystemLoad() throws InterruptedException {
		
		CouponSystem CS = CouponSystem.getInstance();
		Thread.sleep(3000);
		CouponClientFacade cl = 
				CS.login(ServerAdmin.USERNAME, ServerAdmin.PASSWORD, ClientType.ADMIN);
		
		if ( cl == null ) {
			System.out.println("Error: no instance gotten from CouponSystem");
			System.exit(1);
		}
	}
	
	public static Date getCurrentDate() {
		return new Date( new java.util.Date().getTime() );
	}
	
	public static Date getCustomDate( long ms ) {
		Date date = getCurrentDate();
		date.setTime(date.getTime() + ms);
		return date;
	}
	
	public static void basicSanity() throws InterruptedException {
		
		System.out.println("\n\nSTARTING BASIC SANITY TEST..");
		_wait(5);
		
		adminFacade = null;
		compFacades = new CompanyFacade[10];
		custFacades = new CustomerFacade[10];
		companies = new Company[10];
		customers = new Customer[10];
		coupons = new Coupon[20];
		
		CS = CouponSystem.getInstance();
		_wait(4);
		System.err.print("Logging in as Admin (" + ServerAdmin.USERNAME + ":" + ServerAdmin.PASSWORD + ")");
		CouponClientFacade cl = 
				CS.login(ServerAdmin.USERNAME, ServerAdmin.PASSWORD, ClientType.ADMIN);
		
		if ( cl == null ) {
			System.out.println("Error: no instance gotten from CouponSystem");
			System.exit(1);
		}
		System.out.println("\t-\t\t\tSUCCESS");
		System.err.println("DB TYPE CHOSEN IS: " + ((SqlDB.DBVENDOR == null)? "XML DB" : "SQL DB") );
		_wait(1);
		
		System.err.print("Purging DB..");
		if ( FactoryDao.getClassname().contains("xml") )
			prugeXMLFiles();
		else
			purgeDatabase();
		
		Date sqlStartDate = getCurrentDate();
		Date sqlEndDate = getCustomDate(31540000000L); // create one year from now

		System.err.print("Creating all java beans..(10 in total)");
		_wait(1);
		companies[IBM] = new Company(CompanyUtils.getNextID(),"IBM","Pass@111","support@ibm.com", null, null);
		companies[INTEL] = new Company(CompanyUtils.getNextID(),"Intel","Pass@222","support@intel.com", null, null);
		companies[GOOG] = new Company(CompanyUtils.getNextID(),"Google","Pass@333","support@google.com", null, null);
		companies[FBOOK] = new Company(CompanyUtils.getNextID(),"Facebook","Pass@444","support@facebook.com", null, null);
		companies[HP] = new Company(CompanyUtils.getNextID(),"HP","Pass@555","support@hp.com", null, null);
		
		customers[YANIV] = new Customer(CustomerUtils.getNextID(), "Yaniv", "Yaniv@123", "yaniv@yaniv.com", null, null);
		customers[MOSHE] = new Customer(CustomerUtils.getNextID(), "Moshe", "Moshe@123", "moshe@Moshe.com", null, null);
		customers[ADAM] = new Customer(CustomerUtils.getNextID(), "Adam", "Adam@123", "adam@Adam.com", null, null);
		customers[DAVID] = new Customer(CustomerUtils.getNextID(), "David", "David@123", "david@David.com", null, null);
		customers[IZIK] = new Customer(CustomerUtils.getNextID(), "Izik", "Izik@123", "izik@Izik.com", null, null);
		
		coupons[0] = new Coupon(CouponUtils.getNextID(),"Vacation to London", sqlStartDate, sqlEndDate,
				100, CouponType.TRAVELLING, "Congrats you won", 105.25, "com/test/test");
		coupons[1] = new Coupon(CouponUtils.getNextID(),"50% Kitchen Electric discount", sqlStartDate, sqlEndDate,
				150, CouponType.ELECTRICITY, "Congrats you won", 119.25, "com/test/test2");
		coupons[2] = new Coupon(CouponUtils.getNextID(),"MegaSuper 100 points", sqlStartDate, sqlEndDate,
				200, CouponType.FOOD, "Congrats you won", 111.25, "com/test/test3");
		coupons[3] = new Coupon(CouponUtils.getNextID(),"SPA and Massage", sqlStartDate, sqlEndDate,
				250, CouponType.HEALTH, "Congrats you won", 105.25, "com/test/test");
		coupons[4] = new Coupon(CouponUtils.getNextID(),"Vacation to Himalayas", sqlStartDate, sqlEndDate,
				300, CouponType.CAMPING, "Congrats you won", 119.25, "com/test/test2");
		coupons[5] = new Coupon(CouponUtils.getNextID(),"Meal for two", sqlStartDate, sqlEndDate,
				350, CouponType.RESTAURANT, "Congrats you won", 258.25, "com/test/test3");
		coupons[6] = new Coupon(CouponUtils.getNextID(),"MegaSport 100 points", sqlStartDate, sqlEndDate,
				400, CouponType.SPORTS, "Congrats you won", 113.25, "com/test/test");
		coupons[7] = new Coupon(CouponUtils.getNextID(),"Vacation to Vena", sqlStartDate, sqlEndDate,
				450, CouponType.TRAVELLING, "Congrats you won", 311.25, "com/test/test2");
		coupons[8] = new Coupon(CouponUtils.getNextID(),"Vacation to Paris", sqlStartDate, sqlEndDate,
				500, CouponType.TRAVELLING, "Congrats you won", 178.25, "com/test/test3");
		//should be malformed Coupon - description is too long
		coupons[9] = new Coupon(CouponUtils.getNextID(),"test001test001test001test001test001test001test00"
				+ "1test001test001test001test001test001test001test001test001test001test001test001test001te"
				+ "st001test001test001test001", sqlStartDate, sqlEndDate,
				487, CouponType.TRAVELLING, "Congrats you won", 111.25, "com/test/test3");
		
		printLine();
		_wait(1);
		
		adminFacade = (AdminFacade)cl;
		
		System.err.print("Inserting 5 companies into DB");
		adminFacade.createCompany(companies[IBM]);
		adminFacade.createCompany(companies[INTEL]);
		adminFacade.createCompany(companies[GOOG]);
		adminFacade.createCompany(companies[FBOOK]);
		adminFacade.createCompany(companies[HP]);
		System.err.println("\nPulling 5 just inserted companies from DB");
		_wait(1);
		printCompanies();
		_wait(1);
		System.err.print("\nInserting 5 Customers into DB");
		adminFacade.createCustomer(customers[YANIV]);
		adminFacade.createCustomer(customers[MOSHE]);
		adminFacade.createCustomer(customers[ADAM]);
		adminFacade.createCustomer(customers[DAVID]);
		adminFacade.createCustomer(customers[IZIK]);
		System.err.print("\nPulling 5 just inserted customers from DB");
		_wait(1);
		printLine();
		printCustomers();
		System.err.print("\nIBM logging in with correct credentials");
		_wait(1);
		cl = CS.login("IBM", "Pass@111", ClientType.COMPANY);
		if ( cl == null ) {
			System.out.println("ERROR - Could not get instance from Coupon System");
			System.exit(1);
		}
		System.out.println("\t-\t\t\tSUCCESS");
		compFacades[IBM] = (CompanyFacade)cl;
		printLine();
		_wait(1);
		System.err.print("Intel loggin in with correct credentials");
		cl = CS.login("Intel", "Pass@222", ClientType.COMPANY);
		if ( cl == null ){
			System.out.println("ERROR: could not get isntance from coupon system");
			System.exit(1);
		}
		System.out.println("\t-\t\t\tSUCCESS");
		compFacades[INTEL] = (CompanyFacade)cl;
		printLine();
				
		//******************************************
		_wait(1);
		System.err.print("Yaniv logging in with correct credentials");
		cl = CS.login("Yaniv", "Yaniv@123", ClientType.CUSTOMER);
		if ( cl == null )
			System.exit(1);
		System.out.println("\t-\t\t\tSUCCESS");
		custFacades[YANIV] = (CustomerFacade)cl;
		printLine();
		_wait(1);
		System.err.print("Moshe logging in with correct credentials");
		cl = CS.login("Moshe", "Moshe@123", ClientType.CUSTOMER);
		if ( cl == null )
			System.exit(1);
		System.out.println("\t-\t\t\tSUCCESS");
		custFacades[MOSHE] = (CustomerFacade)cl;
		printLine();
		
		//************************************************
		_wait(1);
		System.err.print("IBM & INTEL inserting 2 coupons each into DB - correctly");
		compFacades[IBM].createCoupon(coupons[0]);
		compFacades[IBM].createCoupon(coupons[1]);
		compFacades[INTEL].createCoupon(coupons[2]);
		compFacades[INTEL].createCoupon(coupons[3]);
		System.err.println("\nGetting 4 Coupons from Coupon DB table created by Companies");
		_wait(1);
		printCompanyCoupons();
		_wait(1);
		System.err.print("\nMoshe & Yaniv buying/inserting 2 coupons each into DB - correctly");
		custFacades[YANIV].purchaseCoupon(coupons[0]);
		custFacades[YANIV].purchaseCoupon(coupons[1]);
		custFacades[MOSHE].purchaseCoupon(coupons[2]);
		custFacades[MOSHE].purchaseCoupon(coupons[3]);
		System.err.println("\nGetting 4 Coupons from JOIN DB table purchased by customers");
		_wait(1);
		printCustomerCoupons();
		_wait(1);
		System.err.print("\nIBM Removing coupon[0]");
		compFacades[IBM].removeCoupon(coupons[0]);
		printLine();
		_wait(1);
		System.err.print("\nINTEL Removing coupon[2]");
		compFacades[INTEL].removeCoupon(coupons[2]);
		printLine();
		System.err.println("\nGetting Companies' coupons from DB - after remove operation");
		_wait(1);
		printCompanyCoupons();
		_wait(1);
		System.err.print("\nAdmin removing customer[YANIV]");
		adminFacade.removeCustomer(customers[YANIV]);
		System.out.println("\t-\t\t\tSUCCESS");
		System.err.println("Fetching customer from DB");
		_wait(1);
		printCustomers();
		_wait(1);
		printLine();
		System.err.print("\nGoogle loggin in with INCORRECT credentials\n");
		cl = CS.login("Google", "Pass@222", ClientType.COMPANY);
		if ( cl == null )
			System.out.println("\tERROR LOGGIN:-\t\t\tSUCCESS");
		else
			System.err.println("\tLOGIN SUCCESS:-\t\t\tFAILED");
		
		
		printLine();
		System.err.print("\nHP logging in with correct credentials");
		_wait(1);
		cl = CS.login("HP", "Pass@555", ClientType.COMPANY);
		if ( cl == null ) {
			System.out.println("ERROR - Could not get instance from Coupon System");
			System.exit(1);
		}
		System.out.println("\t-\t\t\tSUCCESS");
		compFacades[HP] = (CompanyFacade)cl;
		printLine();
		_wait(1);
		
		System.err.println("HP inserting 1 coupon with ILLEGAL coupon into DB - name too long");
		compFacades[IBM].createCoupon(coupons[9]);
		System.out.println("\n\n***Check log for operation result");
		printLine();	
		System.err.print("David logging in with correct credentials");
		cl = CS.login("David", "David@123", ClientType.CUSTOMER);
		if ( cl == null )
			System.exit(1);
		System.out.println("\t-\t\t\tSUCCESS");
		custFacades[DAVID] = (CustomerFacade)cl;
		printLine();
		
		
		System.out.println("LOGS located in " + FileNames.LOGS_DIR + " dir");
		System.out.println("PROPERTY located in " + FileNames.PROPERTY_DIR + " dir");
		System.out.println("IN CASE USING XMLDB - located in " + XmlDB.XMLDB_DIR + " dir");
		
	
		System.out.println("\nEND OF BASIC SANITY TEST..");
		System.out.println("\nCOUPON EXPIRATION TASK STILL RUNNING AS A THREAD");
	
	}
	
	public static void printLine(){
		System.out.println("\n=============================================================================================");
	}
	
	public static void main(String[] args) {
		
		//******************888
		try {
			//couponSystemLoad();
			//purgeDatabase();
			basicSanity();
			//testTableCreation();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}
}
