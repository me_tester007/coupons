package com.project.daofile;

/**
 * Log Type for Logger class
 * */
public enum LogType {
	UNDEFINED, COMPANY, CUSTOMER, COUPON, INBOX, ALL
}
