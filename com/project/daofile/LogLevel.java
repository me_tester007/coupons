package com.project.daofile;

/**
 * ENUM for log level
 * */
public enum LogLevel {
	UNDEFINED, FATAL, ERROR, WARNING, INFO, DEBUG
}
