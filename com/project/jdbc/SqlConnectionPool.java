package com.project.jdbc;

import java.sql.Connection;

/**
 * Interface for JDBC connection pool
 * */
public interface SqlConnectionPool {

	public Connection getConnection();
	public void returnConnection( Connection conn );
	public void closeAllConnections();
	
}
