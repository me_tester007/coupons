package com.project.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

import com.project.CouponSystem;
import com.project.constants.JdbcPool;
import com.project.constants.SqlDB;
import com.project.exceptions.sql.SqlRunTimeException;
import com.project.utils.SqlExceptionUtils;

/**
 * Singelton - private constructor, private refference to its single object 
 * and public function that returns the refference to the single instance
 */
public class ConnectionPool implements SqlConnectionPool {

	// initiate the first and single instance of the connection pool
	private static final ConnectionPool _cp = new ConnectionPool();

	// Vector is required for synchronized 
	private Vector<Connection> _availableConns;
	private Vector<Connection> _busyConns;

	private ConnectionPool() {
		try {
			System.out.println("Trying to establish conn...");
			_initializeConnectionPool();
		} catch ( SqlRunTimeException e ) {
			//_cp = null;
			SqlExceptionUtils.connectionFail(e);
			System.err.println(e.getMessage());
			if ( CouponSystem.getInstance() != null )
				CouponSystem.getInstance().shutdown();
			System.exit(1);
		} finally {

		}
	}

	// External usage requires getInstance function for using the singelton
	public static ConnectionPool getInstance() {
		return _cp;
	}

	// private function - initialized only once when the singelton is created
	private void _initializeConnectionPool() throws SqlRunTimeException {
		try {
			Class.forName(SqlDB.DRV_NAME);
		} catch(ClassNotFoundException cnfe) {
			throw new SqlRunTimeException("Can't find class for driver: " + SqlDB.DRV_NAME, SqlRunTimeException.CLASS_LOAD_FAIL);
		}
		_availableConns = new Vector<>(JdbcPool.INIT_SIZE);
		_busyConns = new Vector<>();

		for ( int i = 0; i < JdbcPool.INIT_SIZE; i++ )
			_availableConns.addElement(createNewConnection());

	}

	private Connection createNewConnection() throws SqlRunTimeException {
		// Establish network connection to database
		try {
			return DriverManager.getConnection( SqlDB.URL, SqlDB.USERNAME, SqlDB.PASSWORD );
		} catch (SQLException e) {
			throw new SqlRunTimeException(e.getMessage(), SqlRunTimeException.CONNECT_FAIL);
		}
	}

	@Override
	public synchronized Connection getConnection() throws SqlRunTimeException {
		// return available connection from the connection pool
		if ( _availableConns.isEmpty() )
			if ( getTotalConnections() >= JdbcPool.MAX_SIZE )
				try {
					wait();
				} catch (InterruptedException e) {
					throw new SqlRunTimeException("Interrupted " + e.getMessage());
				}
			else {
				_busyConns.addElement(createNewConnection());
				return _busyConns.lastElement();
			}
		Connection currentConn = _availableConns.lastElement();
		int lastIndex = _availableConns.size() - 1;
		_availableConns.removeElementAt(lastIndex);
		_busyConns.addElement(currentConn);
		try {
			if ( currentConn.isClosed() ) {
				_busyConns.removeElement(currentConn);
				_busyConns.addElement(createNewConnection());
				return _busyConns.lastElement();
			}
		} catch (SQLException e) {
			throw new SqlRunTimeException("DB access error: " + e.getMessage());
		}
		return currentConn;
	}

	@Override
	public synchronized void returnConnection( Connection conn ) {
		_availableConns.addElement(conn);
		_busyConns.removeElement(conn);
		// Wake up all threads that are waiting for a connection
		notifyAll(); 
	}

	@Override
	public synchronized void closeAllConnections() {
		closeConnections(_availableConns);
		_availableConns = new Vector<>();
		closeConnections(_busyConns);
		_busyConns = new Vector<>();
	}	

	private void closeConnections( Vector<Connection> conns ) {
		try {
			for( int i = 0; i < conns.size(); i++ ) {
				Connection connection = conns.elementAt(i);
				if (!connection.isClosed()) {
					connection.close();
				}
			}
		} catch(SQLException sqle) {
			// Ignore errors; garbage collect anyhow
		}
	}

	private int getTotalConnections(){
		return _availableConns.size() + _busyConns.size();
	}

	@Override
	public synchronized String toString() {
		return "ConnectionPool [_availableConns=" + _availableConns + "]";
	}

}