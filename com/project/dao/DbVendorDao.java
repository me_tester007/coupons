package com.project.dao;

/**
 * ENUM TYPE FOR DB VENDORS
 * 	
 * */
public enum DbVendorDao {

	UNDEFINED, MYSQL, POSTGRES, ORACLE, SQL, IBMDB2, SAP
	
}
