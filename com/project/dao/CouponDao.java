package com.project.dao;

import java.util.Collection;

import com.project.beans.Coupon;
import com.project.beans.CouponType;

/**
 * DAO interface for  Coupon DAO object
 * */
public interface CouponDao {

	public void createCoupon( Coupon c );
	public void removeCoupon( Coupon c );
	public void updateCoupon( Coupon c );
	public Coupon getCoupon( int id );
	public Collection<Coupon> getAllCoupons();
	public Collection<Coupon> getCouponByType( CouponType type );
	public void assignCouponToOwner ( int ownerID, int couponID, String table );
	public void removeCouponFromOwner( int couponID, String table );
	public void removeCouponOwner( int ownerID, String colName, String table );
	public int getCouponOwnerID( Coupon coupon, String table );
	public int getCouponLatestID();
	
}
