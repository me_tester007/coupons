package com.project.dao;

import java.util.Collection;

import com.project.beans.Company;
import com.project.beans.Coupon;
import com.project.beans.Inbox;

/**
 * DAO interface for  Company DAO object
 * */
public interface CompanyDao {

	public void createCompany( Company c );
	public void removeCompany( Company c );
	public void updateCompany( Company c );
	public Company getCompany( int id );
	public Company getCompany( String name );
	public Collection<Company> getAllCompanies();
	public Collection<Coupon> getCoupons( int compId );
	public Collection<Inbox> getInboxMessages( int compId );
	public boolean login( String compName, String pwd );
	public int getCompanyLatestID();
	
}
