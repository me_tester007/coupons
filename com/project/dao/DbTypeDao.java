package com.project.dao;

/**
 * ENUM TYPE FOR TYPE OF DATA STORAGE
 * */
public enum DbTypeDao {

	UNDEFINED, DB, XML, JSON

}
