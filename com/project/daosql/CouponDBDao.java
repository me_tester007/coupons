package com.project.daosql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;

import com.project.beans.Coupon;
import com.project.beans.CouponType;
import com.project.constants.SqlDB;
import com.project.dao.CouponDao;
import com.project.exceptions.sql.CouponSqlException;
import com.project.jdbc.ConnectionPool;

/**
 * DAO Layer Class working with MySQL DB server
 * */
public class CouponDBDao implements CouponDao {
	
	private ConnectionPool _pool;
	
	/**
	 * parameterless Class Consturtor that initializes _pool variable to receive<br/>
	 * the ConnectionPool Singleton instance
	 * */
	public CouponDBDao() {
		_pool = ConnectionPool.getInstance();
	}

	/**
	 * Creating 'Coupon' entry in the coupon DB Table
	 * @return void
	 * */
	@Override
	public void createCoupon( Coupon coupon ) throws CouponSqlException {
		if ( coupon == null )
			return;
		
		String query = " insert into "+ SqlDB.COUPON_TABLE +" () values (?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = null;
		Connection _conn = null;
	
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			prepareCouponStatement(ps, coupon);
			ps.execute();
			_conn.commit();
		} catch ( SQLException e ) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new CouponSqlException(e1.getMessage(), CouponSqlException.SQL_ERROR);
				}
			throw new CouponSqlException(e.getMessage(), CouponSqlException.FAIL_TO_INSERT);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * Removing 'Coupon' entry from the coupon DB Table
	 * @return void
	 * */
	@Override
	public void removeCoupon( Coupon coupon ) throws CouponSqlException {
		if ( coupon == null )
			return;
		
		String query = " delete from "+ SqlDB.COUPON_TABLE +" where id=" + coupon.getID() + " limit 1";
		PreparedStatement ps = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new CouponSqlException(e1.getMessage(), CouponSqlException.SQL_ERROR);
				}
			throw new CouponSqlException(e.getMessage(), CouponSqlException.REMOVE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * Updating 'Coupon' Table's specific entry in the coupon DB table
	 * @return void
	 * */
	@Override
	public void updateCoupon( Coupon coupon ) throws CouponSqlException {
		if ( coupon == null )
			return;
		
		String query = "update "+ SqlDB.COUPON_TABLE +" set title=?, start_date=?, end_date=?, "
				+ "amount=?, type=?, message=?, price=?, image=? where id=?";
		PreparedStatement ps = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			updateCouponStatement(ps, coupon);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new CouponSqlException(e1.getMessage(), CouponSqlException.SQL_ERROR);
				}
			throw new CouponSqlException(e.getMessage(), CouponSqlException.UPDATE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * Retreiving specific 'Coupon' entry from the coupon DB Table using coupon ID
	 * @return {@link Coupon}
	 * */
	@Override
	public Coupon getCoupon( int couponID ) throws CouponSqlException {
		if ( couponID <= 0 )
			return null;
		
		Coupon coupon = null;
		String query = " select * from "+ SqlDB.COUPON_TABLE +" where id=" + couponID;
		Statement st = null;
		ResultSet rs = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			if ( ! rs.next() )
				throw new SQLException("No coupon with " + couponID + " found");
			coupon = deserializeCoupon( rs );
		} catch (SQLException e) {
			throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return coupon;
	}

	/**
	 * Requesting all coupons from the coupon DB Table
	 * @return {@link Coupon} Collection
	 * */
	@Override
	public Collection<Coupon> getAllCoupons() throws CouponSqlException {
		Collection<Coupon> coupons = null;
		
		String query = " select * from " + SqlDB.COUPON_TABLE;
		Statement st = null;
		ResultSet rs = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			coupons = deserializeCoupons( rs );
		} catch (SQLException e) {
			throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return coupons;
	}

	/**
	 * Requesting all coupons with specific type from the coupon DB Table
	 * @return {@link Coupon} Collection
	 * */
	@Override
	public Collection<Coupon> getCouponByType( CouponType type ) throws CouponSqlException {
		if ( type == null || type == CouponType.UNDEFINED )
			return null;
		
		String query = " select * from "+ SqlDB.COUPON_TABLE +" where type='" + type.toString().toLowerCase() + "'";
		Collection<Coupon> coupons = null;
		Statement st = null;
		ResultSet rs = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			coupons = deserializeCoupons(rs);
		} catch (SQLException e) {
			throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return coupons;
	}

	/**
	 * Updates the customer JOIN db tabla On purchasing a coupon by Customer<br/>
	 * @return void 
	 * */
	public void assignCouponToOwner ( int ownerID, int couponID, String table ) throws CouponSqlException {
		if ( ownerID <= 0 || couponID <= 0 || table == null )
			return;
		
		String query = " insert into " + table + " () values (" + ownerID + "," + couponID + ")";
		PreparedStatement ps = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new CouponSqlException(e1.getMessage(), CouponSqlException.SQL_ERROR);
				}
			throw new CouponSqlException(e.getMessage(), CouponSqlException.FAIL_TO_INSERT);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}

	/**
	 * Removing entry from JOIN tables in case coupon was expired or owner was removed
	 * @return void
	 * */
	@Override
	public void removeCouponFromOwner( int couponID, String table ) throws CouponSqlException {
		if ( couponID <= 0 || table == null )
			return;
		
		String query = " delete from " + table + " where coupon_id=" + couponID;
		PreparedStatement ps = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new CouponSqlException(e1.getMessage(), CouponSqlException.SQL_ERROR);
				}
			throw new CouponSqlException(e.getMessage(),CouponSqlException.REMOVE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}
	
	/**
	 * Remove the owner (Company \ Customer) from db table
	 * */
	public void removeCouponOwner( int ownerID, String ownerName, String table ) throws CouponSqlException {
		if ( ownerID <= 0 || ownerName == null || table == null )
			return;
		
		String query = " delete from " + table + " where " + ownerName + "=" + ownerID;
		PreparedStatement ps = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			_conn.setAutoCommit(false);
			ps = _conn.prepareStatement(query);
			ps.execute();
			_conn.commit();
		} catch (SQLException e) {
			if ( _conn != null )
				try {
					_conn.rollback();
				} catch (SQLException e1) {
					throw new CouponSqlException(e1.getMessage(), CouponSqlException.SQL_ERROR);
				}
			throw new CouponSqlException(e.getMessage(), CouponSqlException.REMOVE_FAILED);
		} finally {
			try {
				if ( ps != null )
					ps.close();
				_conn.setAutoCommit(true);
			} catch (SQLException e) {
				throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
			}
			_pool.returnConnection(_conn);
		}
	}
	
	/**
	 * this method is used to get initial coupon id after sysem start/restart<br/>
	 * in order to contiune the id numbering<br/>
	 * @return int
	 * */
	public int getCouponLatestID() throws CouponSqlException {
		int index = -1;
		Statement st = null;
		ResultSet rs = null;

		String query = "SELECT * FROM coupon order by id DESC limit 1";
		Connection _conn = null;

		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			if ( rs.next() )
				index = rs.getInt(1);
		} catch (SQLException e) {
			throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new CouponSqlException(e.getMessage(), CouponSqlException.SQL_ERROR);
				}
			_pool.returnConnection(_conn);
		}
		return index;
	}
	
	/**
	 * Search for any given coupon in any given table and returns the ownerID (CompanyID \ CustomerID) of that coupon<br/>
	 * in case coupon found, and return -1 in case coupon not found
	 * @return ownerID of the coupon
	 * */
	@Override
	public int getCouponOwnerID( Coupon coupon, String table ) throws CouponSqlException {
		if ( coupon == null || table == null )
			return -1;
		
		int ownerID = -1; //will be eather customer or company id
		String query = " select * from " + table + " where coupon_id=" + coupon.getID();
		Statement st = null;
		ResultSet rs = null;
		Connection _conn = null;
		
		try {
			_conn = _pool.getConnection();
			st = _conn.createStatement();
			rs = st.executeQuery(query);
			if ( rs.next() )
				ownerID = rs.getInt(1); 
		} catch (SQLException e) {
			throw new CouponSqlException(e);
		} finally {
			if ( st != null )
				try {
					st.close();
				} catch (SQLException e) {
					throw new CouponSqlException(e);
				}
			_pool.returnConnection(_conn);
		}
		return ownerID;
	}
	
	/*
	 * Auxiliary sql SET function that CREATES the query by performing sql prepared statement<br/>
	 * replacig '?' with actualy coupon's variables
	 * 
	 * */
	private void prepareCouponStatement( PreparedStatement ps, Coupon coupon ) throws SQLException {
		ps.setInt(1, coupon.getID()); ps.setString(2, coupon.getTitle());
		ps.setDate(3, coupon.getStartDate()); ps.setDate(4, coupon.getEndDate());
		ps.setInt(5, coupon.getAmount()); ps.setString(6, coupon.getType().toString());
		ps.setString(7, coupon.getMessage()); ps.setDouble(8, coupon.getPrice());
		ps.setString(9, coupon.getImage());
	}
	
	/*
	 * Auxiliary sql SET function that UPDATES the query by performing sql prepared statement<br/>
	 * replacig '?' with actualy coupon's variables
	 * 
	 * */
	private void updateCouponStatement( PreparedStatement ps, Coupon coupon ) throws SQLException {
		ps.setString(1, coupon.getTitle()); ps.setDate(2, coupon.getStartDate()); 
		ps.setDate(3, coupon.getEndDate()); ps.setInt(4, coupon.getAmount()); 
		ps.setString(5, coupon.getType().toString()); ps.setString(6, coupon.getMessage()); 
		ps.setDouble(7, coupon.getPrice()); ps.setString(8, coupon.getImage());
		ps.setInt(9, coupon.getID());
	}
	
	/*
	 * Auxiliary function that reconstructs the coupon object using data from sql table<br/>
	 * */
	private Coupon deserializeCoupon( ResultSet rs ) throws SQLException {
		if ( rs == null )
			return null;
		
		return new Coupon( rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getDate(4), rs.getInt(5),
				CouponType.valueOf(rs.getString(6).toUpperCase()), rs.getString(7), rs.getDouble(8), rs.getString(9));
	}
	
	/*
	 * Auxiliary function that builds the coupon Collection using data from sql table<br/>
	 * */
	private Collection<Coupon> deserializeCoupons( ResultSet rs ) throws SQLException {
		if ( rs == null )
			return null;
		
		Collection<Coupon> coupons = new HashSet<>();
		while ( rs.next() )
			coupons.add( deserializeCoupon(rs) );
		
		if ( coupons.size() == 0 )
			return null;
		return coupons;
	}
	
	
}
