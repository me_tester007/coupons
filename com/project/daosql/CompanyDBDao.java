package com.project.daosql;

import java.util.Collection;
import java.util.HashSet;

import com.project.beans.BeanClassType;
import com.project.beans.Company;
import com.project.beans.Coupon;
import com.project.beans.Inbox;
import com.project.beans.Owner;
import com.project.constants.SqlDB;
import com.project.dao.CompanyDao;
import com.project.exceptions.GenRunTimeException;
import com.project.exceptions.sql.OwnerSqlException;

/**
 * DAO Layer Class working with MySQL DB server
 * */
public class CompanyDBDao implements CompanyDao {

	/**
	 * parameterless Class Consturtor
	 * */
	public CompanyDBDao() {}
	
	/**
	 * Creating 'Company' entry in the company DB Table
	 * @return void
	 * */
	@Override
	public void createCompany( Company company ) throws OwnerSqlException {
		if ( company == null )
			return;

		DaoSqlHandler.createOwner(company, SqlDB.COMPANY_TABLE);
	}

	/**
	 * Removing 'Company' entry from the company DB Table
	 * @return void
	 * */
	@Override
	public void removeCompany( Company company ) throws OwnerSqlException {
		if ( company == null )
			return;
		
		DaoSqlHandler.removeOwner(company, SqlDB.COMPANY_TABLE);
	}

	/**
	 * Updating 'Company' Table's specific entry in the company DB table
	 * @return void
	 * */
	@Override
	public void updateCompany( Company company ) throws OwnerSqlException {
		if ( company == null )
			return;
		
		DaoSqlHandler.updateOwner(company, SqlDB.COMPANY_TABLE);
	}

	/**
	 * Requesting specific 'Company' entry from the company Table using CompanyID
	 * @return {@link Company}
	 * */
	@Override
	public Company getCompany( int id ) throws OwnerSqlException, GenRunTimeException {
		if ( id <= 0  )
			return null;
		
		return (Company)DaoSqlHandler.getOwner(id, SqlDB.COMPANY_TABLE, BeanClassType.COMPANY);
	}
	
	/**
	 * Retrieving specific 'Company' entry from the company Table using Company Name
	 * @return Company
	 * */
	@Override
	public Company getCompany( String name ) throws OwnerSqlException, GenRunTimeException {
		if ( name == null  )
			return null;
		
		return (Company)DaoSqlHandler.getOwner(name, SqlDB.COMPANY_TABLE, BeanClassType.COMPANY);
	}

	/**
	 * Retrieving all companies from company table
	 * @return {@link Company} Collection
	 * */
	@Override
	public Collection<Company> getAllCompanies() throws OwnerSqlException {
		
		Collection<Owner> owners = DaoSqlHandler.getAllOwners(SqlDB.COMPANY_TABLE, BeanClassType.COMPANY);
		Collection<Company> companies = new HashSet<>();
		for (Owner owner : owners)
			companies.add((Company)owner);
		
		return companies;
	}

	/**
	 * Retrieving all Company's related Coupons from the coupons table using Company id
	 * @return {@link Coupon} Collection
	 * */
	@Override
	public Collection<Coupon> getCoupons( int companyID ) throws OwnerSqlException {
		if ( companyID <= 0 )
			return null;
		
		return DaoSqlHandler.getCoupons(companyID, SqlDB.COMPANY_COUPON_TABLE, SqlDB.COMPID_COLUMN);
	}
	
	/**
	 * Retrieving all Company's related messages from the inbox DB table using Company id
	 * @return {@link Inbox} Collection
	 * */
	@Override
	public Collection<Inbox> getInboxMessages( int companyID ) throws OwnerSqlException {
		if ( companyID <= 0 )
			return null;
		
		return DaoSqlHandler.getInboxMessages(companyID, SqlDB.COMPANY_INBOX_TABLE, SqlDB.COMPID_COLUMN);
	}
	
	/**
	 * This function is required to get the initial company id number to start from, </br>
	 * for creating new IDs for coupons.
	 * @return int
	 * */
	public int getCompanyLatestID() throws OwnerSqlException {
		
		return DaoSqlHandler.getOwnerLatestID(SqlDB.COMPANY_TABLE);
	}
	
	/**
	 * Login function verifies access previliges for any specific company<br/>
	 * Returns true if credentials are valid - false otherwise
	 * @return boolean
	 * */
	@Override
	public boolean login( String companyName, String password ) throws OwnerSqlException {
		if ( companyName == null || password == null )
			return false;
		
		return DaoSqlHandler.login(companyName, password, SqlDB.COMPANY_TABLE);
	}
	
	
	
}
