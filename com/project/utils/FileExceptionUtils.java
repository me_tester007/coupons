package com.project.utils;

import com.project.exceptions.file.FileRunTimeException;

/**
 * General Class for handling file related Exceptions
 * */
public class FileExceptionUtils {

	public static final int FILE_NOT_FOUND = 1;
	public static final int UPDATE_FAILED = 2;
	public static final int REMOVE_FAILED = 3;
	public static final int CONNECT_FAIL = 4;
	public static final int CLASS_LOAD_FAIL = 5;
	public static final int SQL_ERROR = 6;
	
	/**
	 * Handle gerenal error occured during file operation
	 * */
	public static void fileException ( FileRunTimeException exception ) {
		System.out.println("File handle error - " + exception.getExceptionMessage());
		if ( exception.getError() == FileRunTimeException.FILE_NOT_FOUND )
			fileException(exception);
	}
	
	/**
	 * Handle file not found exception
	 * */
	public static void fileNotFound ( FileRunTimeException excepton ) {
		
			System.out.println(excepton.getMessage());
	}
	
	
	
	
}
