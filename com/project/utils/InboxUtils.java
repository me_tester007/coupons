package com.project.utils;

import com.project.dao.InboxDao;

/**
 *Class is used for generating the next ID for log by pulling the </br>
 *last row from the Inbox DB table and incrementing it by one for further usage </br>
 *and initialized only once when server starts
 * */
public class InboxUtils {
	
	private static int _idCounter;
	
	static {
		initializeIdCounter();
	}
	
	private InboxUtils(){}
	
	private static void initializeIdCounter() {

		//InboxDao inboxDao = new InboxDBDao();
		InboxDao inboxDao = FactoryDao.getInboxDao();
		
		if ( (_idCounter = inboxDao.getInboxLatestID()) <= 0 )
			_idCounter = 0;
	}
	
	/**
	 * Get the next ID for inbox object
	 * */
	public static int getNextID(){
		return ++_idCounter;
	}

}
