package com.project.utils;

import com.project.dao.CouponDao;

/**
 *Class is used for generating the next ID for coupon by pulling the </br>
 *last row from the coupon DB table and incrementing it by one for further usage </br>
 *and initialized only once when server starts
 * */
public class CouponUtils {

	//private static boolean startup = true;
	private static int _idCounter;
	
	static {
		initializeIdCounter();
	}
	
	private CouponUtils() {}
	
	private static void initializeIdCounter() {
		
		//CouponDao couponDao = new CouponDBDao();
		CouponDao couponDao = FactoryDao.getCouponDao();
		
		if ( (_idCounter = couponDao.getCouponLatestID()) <= 0 )
			_idCounter = 10;
	}
	
	/**
	 * Get next ID for Coupon object
	 * @return int
	 * */
	public static int getNextID(){
		return ++_idCounter;
	}
	
}
