package com.project.utils;

import java.io.IOException;

import javax.management.modelmbean.XMLParseException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * Class that handles the xml exceptions that were caught during runtime
 * */
public class XmlExceptionUtils {
	
	
	private XmlExceptionUtils(){}
	
	/**
	 * Hsndle SAX exception
	 * */
	public static void xmlSaxException( SAXException e ) {
		System.err.println(e.getMessage());
	}
	
	/**
	 * Handle IOException
	 * */
	public static void xmlIOException( IOException e ) {
		System.err.println(e.getMessage());
	}
	
	/**
	 * Handle Parse config exception
	 * */
	public static void xmlParseConfException( ParserConfigurationException e ){
		System.err.println(e.getMessage());
	}
	
	/**
	 * Handle XML Parse exception
	 * */
	public static void xmlParseException( XMLParseException e ){
		System.err.println(e.getMessage());
	}
	
}
