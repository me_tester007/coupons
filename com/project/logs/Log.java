package com.project.logs;

import java.sql.Date;

import com.project.daofile.LogLevel;

/**
 *  Logs bean class represents Log object of Logs DB table
 * */
public class Log {

	private int _id;
	private Date _date;
	private String _descirption;

	/**
	 * Constructor for initializing log object
	 * */
	public Log( int id, Date timeStamp, String description ) {
		_id = id;
		_date = timeStamp;
		_descirption = description;
	}	
	
	/**
	 * Prepare formatted statement that is used within sql INSERT query
	 * @return String
	 */
	public String getQuery() {
		return "(" + _id + ",'" + _date + "','" + _descirption + "')";
	}	
	
	/**
	 * Prepare formatted statement that is used within Logging operation
	 * @return String
	 */
	public String getData( LogLevel logLevel ) {
		return new java.util.Date(_date.getTime()) + " - " + logLevel.toString() + " - " + _descirption;
	}

	//	GETTERS
	public int getID() {
		return _id;
	}
	
	public Date getDate() {
		return _date;
	}
	
	public String getDescirption() {
		return _descirption;
	}
	
	//	SETTERS
	public void setID( int id ) {
		_id = id;
	}
	
	public void setDate( Date timeStamp ) {
		_date = timeStamp;
	}
	
	public void setDescirption( String descirption ) {
		_descirption = descirption;
	}
	
	@Override
	public boolean equals( Object obj ) {
		if ( obj == null ) return false;
		if ( obj == this ) return true;
		if ( ! (obj instanceof Log) ) return false;
		return _id == ((Log)obj)._id;
	}
		
	@Override
	public String toString() {
		return "Log [_id=" + _id + ", _timeStamp=" + _date + ", _descirption=" + _descirption + "]";
	}
	
	
}
